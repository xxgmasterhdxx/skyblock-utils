package com.gmasterhd.skyblockutils.commands;

import com.gmasterhd.skyblockutils.SkyBlockUtils;
import com.gmasterhd.skyblockutils.jsons.KillStat;
import com.gmasterhd.skyblockutils.jsons.PersistentSkill;
import com.gmasterhd.skyblockutils.utils.SkillXP;
import com.gmasterhd.skyblockutils.utils.Toolbox;
import com.google.gson.JsonObject;

import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class CommandAPIImport extends CommandBase {
	@Override
	public String getCommandName() {
		return "sbuapiimport";
	}
	
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/sbuapiimport <moudles>";
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if(args.length > 0) {
			if(args[0].toLowerCase().equals("kills")) {
				sender.addChatMessage(new ChatComponentText("Importing kills from API..."));
				if(SkyBlockUtils.saves.apiKey.equals("")) {
					sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + " no API Key defined! Use /sbuapikey to set your API key. You can get your API Key if you type in /api new"));
					return;
				}
				
				JsonObject skyBlockStats = Toolbox.getSkyBlockStats(sender.getName().toLowerCase());
				JsonObject kills = skyBlockStats.getAsJsonObject("stats");
				
				setKill("Crypt Ghoul", kills.get("kills_unburried_zombie").getAsInt());
				setKill("Zealot", kills.get("kills_zealot_enderman").getAsInt());
				setKill("Enderman", kills.get("kills_enderman").getAsInt());
				setKill("Obsidian Defender", kills.get("kills_obsidian_wither").getAsInt());
				setKill("Watcher", kills.get("kills_watcher").getAsInt());
				setKill("Endermite", kills.get("kills_endermite").getAsInt());
			} else if(args[0].toLowerCase().equals("skills")) {
				JsonObject skyBlockStats = Toolbox.getSkyBlockStats(sender.getName().toLowerCase());
				SkillXP combatXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_combat").getAsDouble());
				SkillXP farmingXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_farming").getAsDouble());
				SkillXP enchantingXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_enchanting").getAsDouble());
				SkillXP alchemyXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_alchemy").getAsDouble());
				SkillXP miningXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_mining").getAsDouble());
				SkillXP foragingXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_foraging").getAsDouble());
				SkillXP tamingXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_taming").getAsDouble());
				SkillXP fishingXP = SkillXP.getLevelByXP(skyBlockStats.get("experience_skill_fishing").getAsDouble());
				
				SkyBlockUtils.saves.Combat = new PersistentSkill("Combat", combatXP.level, combatXP.progress, combatXP.nextLevel - combatXP.progress, 0);
				SkyBlockUtils.saves.Farming = new PersistentSkill("Farming", farmingXP.level, farmingXP.progress, farmingXP.nextLevel - farmingXP.progress, 0);
				SkyBlockUtils.saves.Enchanting = new PersistentSkill("Enchanting", enchantingXP.level, enchantingXP.progress, enchantingXP.nextLevel - enchantingXP.progress, 0);
				SkyBlockUtils.saves.Alchemy = new PersistentSkill("Alchemy", alchemyXP.level, alchemyXP.progress, alchemyXP.nextLevel - alchemyXP.progress, 0);
				SkyBlockUtils.saves.Mining = new PersistentSkill("Mining", miningXP.level, miningXP.progress, miningXP.nextLevel - miningXP.progress, 0);
				SkyBlockUtils.saves.Foraging = new PersistentSkill("Foraging", foragingXP.level, foragingXP.progress, foragingXP.nextLevel - foragingXP.progress, 0);
				SkyBlockUtils.saves.Taming = new PersistentSkill("Taming", tamingXP.level, tamingXP.progress, tamingXP.nextLevel - tamingXP.progress, 0);
				SkyBlockUtils.saves.Fishing = new PersistentSkill("Fishing", fishingXP.level, fishingXP.progress, fishingXP.nextLevel - fishingXP.progress, 0);
			}
		}
	}
	
	public void setKill(String entityName, int value) {
		Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("Importing Kills of " + entityName + "..."));
		boolean found = false;
		for(int x = 0; x < SkyBlockUtils.saves.KillStats.size(); ++x) {
			KillStat ks = SkyBlockUtils.saves.KillStats.get(x);
			
			if(ks.entity.equals(entityName)) {
				found = true;
				SkyBlockUtils.saves.KillStats.set(x, new KillStat(entityName, value));
			}
		}
		
		if(!found) {
			SkyBlockUtils.saves.KillStats.add(new KillStat(entityName, value));
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
