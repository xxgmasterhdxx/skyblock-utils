package com.gmasterhd.skyblockutils.commands;

import com.gmasterhd.skyblockutils.SkyBlockUtils;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;

public class CommandSkillInfo extends CommandBase {
	@Override
	public String getCommandName() {
		return "sbuskillinfo";
	}
	
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/sbuskillinfo true or /sbuskillinfo false";
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if(args.length > 0) {
			if(args[0].toLowerCase().equals("true")) {
				SkyBlockUtils.saves.skillInfoVisible = true;
			} else if(args[0].toLowerCase().equals("false")) {
				SkyBlockUtils.saves.skillInfoVisible = false;
			}
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
