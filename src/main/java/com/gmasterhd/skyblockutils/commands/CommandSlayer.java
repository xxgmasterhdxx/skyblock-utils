package com.gmasterhd.skyblockutils.commands;

import com.gmasterhd.skyblockutils.utils.SlayerXP;
import com.gmasterhd.skyblockutils.utils.Toolbox;
import com.google.gson.JsonObject;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class CommandSlayer extends CommandBase {
	@Override
	public String getCommandName() {
		return "sbslayer";
	}
	
	@Override
	public String getCommandUsage(ICommandSender sender) {
		return "/sbuslayer <username>";
	}
	
	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		if(args.length >= 1) {
			JsonObject skyblockStats = Toolbox.getSkyBlockStats(args[0]);
			SlayerXP zombie = SlayerXP.getLevelByXP(skyblockStats.getAsJsonObject("slayer_bosses").getAsJsonObject("zombie").get("xp").getAsInt());
			SlayerXP spider = SlayerXP.getLevelByXP(skyblockStats.getAsJsonObject("slayer_bosses").getAsJsonObject("spider").get("xp").getAsInt());
			SlayerXP wolf = SlayerXP.getLevelByXP(skyblockStats.getAsJsonObject("slayer_bosses").getAsJsonObject("wolf").get("xp").getAsInt());
			
			sender.addChatMessage(new ChatComponentText("Slayer Stats by " + args[0]));
			sender.addChatMessage(new ChatComponentText("Revenant Horror: Lvl. " + zombie.level + ", " + zombie.progress + " / " + zombie.nextLevel));
			sender.addChatMessage(new ChatComponentText("Tarantula Broodfather: Lvl. " + spider.level + ", " + spider.progress + " / " + spider.nextLevel));
			sender.addChatMessage(new ChatComponentText("Sven Packmaster: Lvl. " + wolf.level + ", " + wolf.progress + " / " + wolf.nextLevel));
		}
	}
	
	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		return true;
	}
}
