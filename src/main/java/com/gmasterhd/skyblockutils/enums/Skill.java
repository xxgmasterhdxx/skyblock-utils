package com.gmasterhd.skyblockutils.enums;

public enum Skill {
	COMBAT,
	FORAGING,
	MINING,
	TAMING,
	ENCHANTING,
	ALCHEMY,
	FISHING,
	FARMING
}
