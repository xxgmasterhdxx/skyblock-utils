package com.gmasterhd.skyblockutils.jsons;

import java.util.ArrayList;
import java.util.List;

public class Slayer {
	public String slayerName;
	public List<SlayerItem> items = new ArrayList();
	public int kills;
	
	public Slayer(String name) {
		this.slayerName = name;
	}
	public Slayer(String name, int kills) {
		this.slayerName = name;
		this.kills = kills;
	}
}
