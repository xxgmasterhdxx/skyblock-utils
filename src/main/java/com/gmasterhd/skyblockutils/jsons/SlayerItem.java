package com.gmasterhd.skyblockutils.jsons;

public class SlayerItem {
	public String name;
	public int count;
	
	public SlayerItem(String name, int count) {
		this.name = name;
		this.count = count;
	}
}
