package com.gmasterhd.skyblockutils.utils;

public class EntityHandler {
	public static String getEntityImage(String entity) {
		if(entity.equals("Zealot") || entity.equals("Enderman")) {
			return "skyblockutils:textures/icons/enderman.png";
		} else if(entity.equals("Wolf") || entity.equals("Soul of the Alpha") || entity.equals("Pack Spirit") || entity.equals("Old Wolf") || entity.equals("Sven Pack Enforcer") || entity.equals("Alpha")) {
			return "skyblockutils:textures/icons/wolf.png";
		} else if(entity.equals("Obsidian Defender")) {
			return "skyblockutils:textures/icons/obsidian_defender.png";
		} else if(entity.equals("Crypt Ghoul") || entity.equals("Revenant Sycophant") || entity.equals("Revenant Champion") || entity.equals("Deformed Revenant")) {
			return "skyblockutils:textures/icons/zombie.png";
		} else if(entity.equals("Spider") || entity.equals("Dasher Spider") || entity.equals("Splitting Spider") || entity.equals("Voracious Spider")) {
			return "skyblockutils:textures/icons/spider.png";
		} else {
			return "skyblockutils:textures/icons/spider.png";
		}
	}
}
