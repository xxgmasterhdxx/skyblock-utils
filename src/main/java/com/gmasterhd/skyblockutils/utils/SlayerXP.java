package com.gmasterhd.skyblockutils.utils;

import java.util.ArrayList;
import java.util.List;

public class SlayerXP {
	public int level;
	public double progress;
	public int nextLevel;
	
	public SlayerXP(int level, double progress, int nextLevel) {
		this.level = level;
		this.progress = progress;
		this.nextLevel = nextLevel;
	}
	
	public static int slayerXPPerLevel[] = {
			5,
			15,
			200,
			1000,
			5000,
			20000,
			100000,
			400000,
			1000000
	};
	
	public static SlayerXP getLevelByXP(int xp) {
		int level = 0;
		int exp = xp;
		int nextLevel = 0;
		int lastEXP = 0;
		for(int i : slayerXPPerLevel) {
			if(exp >= i) {
				lastEXP = exp;
				++level;
				nextLevel = i;
			}
		}
		if(level == 9) {
			nextLevel = 0;
		}
		if(nextLevel >= 9) {
			nextLevel = slayerXPPerLevel[level];
		} else {
			nextLevel = 0;
		}
		return new SlayerXP(level, lastEXP, nextLevel);
	}
	
	@Override
	public String toString() {
		return "Level: " + level + ", Progress: " + progress + ", nextLevel: " + nextLevel;
	}
}
